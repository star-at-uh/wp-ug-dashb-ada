<?php
/*
Plugin Name: UH mods for Total Theme
Plugin URI: http://www.hawaii.edu/its/
Description: UH mods for Total Theme
Version: 0.1
Author: ITS Web Support
Author URI: http://www.hawaii.edu/its/
*/

// remove IDs from WordPress Menus ( https://wpexplorer-themes.com/total/snippets/remove-ids-from-wordpress-menus/ )
add_filter( 'nav_menu_item_id', '__return_false' );


?>
